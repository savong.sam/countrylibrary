using CountryLibrary.API.Models;
using CountryLibrary.API.Repositories;

namespace CountryLibrary.API.Services;

public class CountryService : ICountryService
{
    private readonly ICountryRepository _countryRepository;
    private ICountryService _countryServiceImplementation;

    public CountryService(ICountryRepository countryRepository) {

        _countryRepository = countryRepository;
    }

    public async Task<CountryInfo> GetCountryByName(CountryByNameRequest request)
    {
        var country = await _countryRepository.GetCountryByName(request.CountryByName);
        return country;
    }

    public async Task<List<CountryInfo>> GetCountryByArea(CountryByAreaRequest request)
    {
        var allCountries = await _countryRepository.GetCountryByRegion(request.Region);
        var filterCountries = allCountries.Where(country => country.TimeZones.Contains(request.TimeZones)).ToList();
        return (filterCountries.Count != 0 ? filterCountries : null)!;
    }
}