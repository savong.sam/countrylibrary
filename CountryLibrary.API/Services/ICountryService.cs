using CountryLibrary.API.Models;

namespace CountryLibrary.API.Services;

public interface ICountryService
{
    public Task<CountryInfo> GetCountryByName(CountryByNameRequest name);
    public Task<List<CountryInfo>> GetCountryByArea(CountryByAreaRequest areaInfo);
}