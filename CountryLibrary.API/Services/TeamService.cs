using CountryLibrary.API.Models;

namespace CountryLibrary.API.Services;

public class TeamService : ITeamService
{
    public List<TeamMember> GetTeamMembers()
    {
        return new List<TeamMember>
        {
            new TeamMember
            {
                Name = "Savong",
                Gender = "Male",
                Age = 30,
                Address = "Phonm Penh",
                Email = "alice.johnson@example.com"
            },
            new TeamMember
            {
                Name = "Veha",
                Gender = "Male",
                Age = 35,
                Address = "Phonm Penh", 
                Email = "bob.smith@example.com"
            },
            new TeamMember
            {
                Name = "Manuth",
                Gender = "Male",
                Age = 25,
                Address = "Phonm Penh",
                Email = "charlie.brown@example.com"
            }
        };
    }
}