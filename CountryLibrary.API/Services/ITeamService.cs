using CountryLibrary.API.Models;

namespace CountryLibrary.API.Services;

public interface ITeamService
{
    List<TeamMember> GetTeamMembers();
}