using System.Text.Json;
using CountryLibrary.API.Models;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace CountryLibrary.API.Repositories;

public class CountryRepository : ICountryRepository
{
    private readonly HttpClient _httpClient;
    public CountryRepository() {
        _httpClient = new HttpClient();
        _httpClient.BaseAddress = new System.Uri("https://restcountries.com/v2/");
    }

    public async Task<CountryInfo> GetCountryByName(string countryName)
    {
        var response = await _httpClient.GetAsync($"name/{countryName}");
        if (!response.IsSuccessStatusCode) return null!;
        var json = await response.Content.ReadAsStringAsync();
        var countries = JsonSerializer.Deserialize<List<CountryInfo>>(json, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });

        return countries![0];
    }

    public async Task<List<CountryInfo>> GetCountryByRegion(string area)
    {
        var response = await _httpClient.GetAsync($"region/{area}");
        if (!response.IsSuccessStatusCode) return null!;
        var json = await response.Content.ReadAsStringAsync();
        var allCountries = JsonSerializer.Deserialize<List<CountryInfo>>(json, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
        return allCountries!;
    }
}