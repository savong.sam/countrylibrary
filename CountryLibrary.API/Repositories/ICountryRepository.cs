using CountryLibrary.API.Models;

namespace CountryLibrary.API.Repositories;

public interface ICountryRepository
{
    public Task<CountryInfo> GetCountryByName(string countryByName);
    public Task<List<CountryInfo>> GetCountryByRegion(string area);
}