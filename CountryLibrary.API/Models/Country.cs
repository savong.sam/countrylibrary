using Newtonsoft.Json;

namespace CountryLibrary.API.Models;

public class CountryInfo
{
    public string Name { get; set; }
    public string Alpha2Code { get; set; }
    public string Capital { get; set; }
    public string[] CallingCodes { get; set; }
    public Flag Flags { get; set; }
    public string[] TimeZones { get; set; }
}

public class CountryByNameRequest
{
    public string CountryByName { get; set; }
}

public class CountryByAreaRequest
{
    public string Region { get; set; }
    public string TimeZones { get; set; }
}

public class Flag
{
    public string Svg { get; set; }
    public string Png { get; set; }
}
