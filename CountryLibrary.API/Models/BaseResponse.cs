using System.Text.Json.Serialization;

namespace CountryLibrary.Api.Models;

public class BaseResponse
{
    public string? ErrorMessage { get; set; }
    public int ErrorCode { get; set; }
    public bool IsSuccess => ErrorCode == 0;
    
}