using CountryLibrary.Api.Models;

namespace CountryLibrary.Api.Helpers;

public class ApiResponse<T>: BaseResponse
{
    public T? Data { get; set; }
    
}