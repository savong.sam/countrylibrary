using CountryLibrary.Api.Helpers;
using CountryLibrary.API.Models;
using CountryLibrary.API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CountryLibrary.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamController(ITeamService teamService) : ControllerBase
    {
        [HttpGet("GetTeamMembers")]
        public Task<ApiResponse<List<TeamMember>>> GetTeamMembers()
        {
            return Task.FromResult(result: new ApiResponse<List<TeamMember>>()
            {
                Data = teamService.GetTeamMembers()
            });
        }
    }
}
