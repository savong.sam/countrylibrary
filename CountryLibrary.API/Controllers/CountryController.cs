using CountryLibrary.Api.Helpers;
using CountryLibrary.API.Models;
using CountryLibrary.API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CountryLibrary.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController(ICountryService countryService) : ControllerBase
    {
        [HttpPost("GetCountryByName")]
        public async Task<ActionResult<CountryInfo>> GetCountryByName([FromBody] CountryByNameRequest name)
        {
            var country = await countryService.GetCountryByName(name);
      
            if(country == null)
            {
                return NotFound("No Country found.");
            }
            return Ok(country);
        }
        
        [HttpPost("GetCountryByArea")]
        public async Task<ActionResult<CountryInfo>> GetCountryByArea([FromBody] CountryByAreaRequest request)
        {
            var countryInfos = await countryService.GetCountryByArea(request);

            if(countryInfos == null)
            {
                return NotFound("Data is not found.");
            }
            return Ok(countryInfos);
        }
    }
}
